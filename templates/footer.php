<footer class="site-footer">
  <div class="container">
    <div class="row">

      <div class="col-12">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/images/logo.svg" alt="logo" class="logo" />
      </div>

      <div class="col-lg-4 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-1'); ?>
      </div>

      <div class="col-lg-3 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-2'); ?>
      </div>

      <div class="col-lg-3 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-3'); ?>
      </div>

      <div class="col-lg-2 col-md-6">
        <?php dynamic_sidebar('sidebar-footer-4'); ?>
      </div>

    </div>
  </div>
</footer>
<div class="footer-bar">
  <div class="container">

    <?php get_template_part( 'templates/partials/social-links' ); ?>

    <span class="small credit">
      &copy; Copyright <?php echo date('Y'); ?> - Headstrong Fitness.
    </span>
    <span class="credit credit-link"><a href="https://moshpitdigital.com" target="_blank" class="small">Website By Moshpit Digital</a></span>
  </div>
</div>
