<section class="section">
  <div class="row">

    <div class="col-md-6">
      <?php if ( have_rows( 'hours_list' ) ) : ?>
        <section class="contact-hours">
          <h2 class="text-left">Open Hours</h2>

          <?php while ( have_rows( 'hours_list') ) : the_row(); ?>
            <div class="card hours-card">

              <div class="card-body bg-light">
                <div class="row">

                  <div class="col-2">
                    <div class="d-flex h-100 align-items-center">
                      <p class="text-primary mb-0"><i class="fa fa-clock-o fa-3x" aria-hidden="true"></i></p>
                    </div>
                  </div>

                  <div class="col-10">
                    <h1 class="card-title"><?php the_sub_field( 'days' ); ?></h1>
                    <h4 class="card-subtitle"><?php the_sub_field( 'hours' ); ?></h4>
                  </div>

                </div>
              </div>

            </div>
          <?php endwhile; ?>
        </section>
      <?php endif; ?>

      <?php if ( get_field( 'contact_info' ) ) : ?>
        <section class="contact-info">
          <div class="mt-5">
            <h2 class="text-left">Contact Info</h2>

            <?php the_field( 'contact_info', false, false ); ?>
          </div>
        </section>
      <?php endif; ?>
    </div>

    <div class="col-md-6">
      <section class="contact-form">
        <h2 class="text-left">Contact Us</h2>
        <?php echo do_shortcode( '[gravityform id="1" title="false" description="false" tabindex="100"]' ); ?>
      </section>
    </div>

  </div>
</section>