<header class="site-header">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark">
      <a class="navbar-brand opacity-hover" href="<?= esc_url(home_url('/')); ?>"><img
            src="<?= get_stylesheet_directory_uri(); ?>/dist/images/logo.svg"
            alt="logo" class="logo" /></a>
      <button class="navbar-toggler navbar-toggler-right" type="button"
              data-toggle="collapse" data-target="#bs4navbar"
              aria-controlers="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
            'theme_location'  => 'primary_navigation',
            'container'       => 'div',
            'container_id'    => 'bs4navbar',
            'container_class' => 'collapse navbar-collapse justify-content-end',
            'menu_id'         => false,
            'menu_class'      => 'navbar-nav',
            'depth' => 2,
        ]);
      endif;
      ?>
    </nav>
  </div>
</header>
