<?php use Roots\Sage\Titles; ?>

<?php
// ACF Fields for Page Banner
$page_banner_image = get_field('page_banner_image');
$custom_banner_content_bool = get_field('custom_banner_content_bool');
$page_banner_title = get_field('page_banner_title');
$page_banner_description = get_field('page_banner_description');
$custom_banner_content_editor = get_field('custom_banner_content_editor');
$banner_overlay_bool = get_field('banner_overlay_bool');
$page_header_classes = array( 'page-header' );

if ( $page_banner_image ) {
  array_push( $page_header_classes, 'has-banner' );

  if ( $banner_overlay_bool ) {
    array_push( $page_header_classes, 'has-overlay');
  }
}
?>

<div class="header-bar">
  <div class="container">
    <span id="header-phone">Call or Text <a href="tel:8055502136" class="lead"><strong>(805) 550-2136</strong></a></span>

    <?php get_template_part( 'templates/partials/social-links' ); ?>
  </div>
</div>

<div class="<?php echo implode( $page_header_classes, ' ' ); ?>"
    <?php if ($page_banner_image) : ?>
      style="background-image: url(<?php echo $page_banner_image['url']; ?>)"
    <?php endif; ?>
>

  <?php
  do_action('get_header');
  get_template_part('templates/header');
  ?>

  <div class="banner-content <?php if ( !$page_banner_image ) echo 'mt-4';?>">
    <?php if ( $custom_banner_content_bool ) : ?>

      <?php the_field('custom_banner_content_editor', false, false); ?>

    <?php else : ?>

      <div class="container">
        <?php if ( $page_banner_title ) : ?>
          <h1><?php echo $page_banner_title; ?></h1>
        <?php else : ?>
          <h1><?= Titles\title(); ?></h1>
        <?php endif; ?>

        <?php if ($page_banner_description) : ?>
          <h4><?php echo $page_banner_description; ?></h4>
        <?php endif; ?>
      </div>

    <?php endif; ?>
  </div>
</div>
