<section class="section">
  <?php if ( have_rows( 'services') ) : ?>

      <div class="row">
        <?php while ( have_rows( 'services') ) : the_row(); ?>

            <div class="col-md-4 col-sm-6 mt-4">

              <div class="card h-100 text-center">

                <div class="card-header">
                  <h4 class="card-title text-primary mb-0"><?php the_sub_field( 'service_title' ); ?></h4>
                </div>

                <div class="card-body h-100 d-flex flex-column">
                  <?php if ( have_rows( 'service_list' ) ) : ?>

                    <ul class="pl-0 list-style-position-inside mb-4">
                      <?php while ( have_rows ('service_list' ) ) : the_row(); ?>
                        <li><?php the_sub_field( 'service_list_item' ); ?></li>
                      <?php endwhile; ?>
                    </ul>

                  <?php endif; ?>

                  <div class="mt-auto">
                    <a href="<?php the_sub_field( 'service_link' ); ?>" class="btn btn-primary">Sign Up</a>
                  </div>
                </div>
              </div>

            </div>

        <?php endwhile; ?>
      </div>

  <?php endif; ?>
</section>
