<?php
$topic_background = get_field( 'topics_background' );
$hero_classes = array( 'hero dark-theme' );

if ( $topic_background ) {
  array_push( $hero_classes, 'has-background' );
}
?>

<section class="<?php echo implode( $hero_classes, ' '); ?>"
    <?php if ( $topic_background ) : ?>
      style="background-image: url(<?php echo $topic_background['url']; ?>)"
    <?php endif; ?>
>

  <div class="container hero-content">

    <?php if ( have_rows( 'topics' ) ) : ?>
        <div class="row">

          <?php while ( have_rows( 'topics' ) ) : the_row(); ?>
              <div class="col-lg-4 col-sm-6">
                <div class="row mt-4">

                  <div class="col-sm-3 text-sm-right">
                    <i class="fa fa-3x text-primary <?php the_sub_field( 'topic_icon' ); ?>"></i>
                  </div>

                  <div class="col-sm-9">
                    <h1><?php the_sub_field( 'topic_title' ); ?></h1>
                    <p><small><?php the_sub_field( 'topic_info' ); ?></small></p>
                  </div>

                </div>
              </div>
          <?php endwhile; ?>

        </div>
    <?php endif; ?>

  </div>

</section>
