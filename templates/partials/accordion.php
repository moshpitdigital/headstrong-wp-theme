<?php if ( have_rows( 'accordion' ) ) : ?>
  <div class="accordion-list" id="accordion" role="tablist">
    <?php $accordionCounter = 1; ?>
    <?php while ( have_rows( 'accordion' ) ) : the_row(); ?>

        <div class="accordion-item">

          <div class="accordion-header" role="tab" id="heading-<?php echo $accordionCounter; ?>" >
            <h4 class="accordion-title">
              <a data-toggle="collapse"
                 href="#collapse-<?php echo $accordionCounter; ?>"
                 aria-expanded="true"
                 aria-controls="collapse-<?php echo $accordionCounter; ?>"
              >
                <?php the_sub_field( 'icon' ); ?>&nbsp;<?php the_sub_field( 'title' ); ?>
              </a>
            </h4>
          </div>

          <div class="
            <?php if ( $accordionCounter === 1 ) : ?>
              accordion-body collapse show
            <?php else : ?>
              accordion-body collapse
            <?php endif; ?>
          "
               id="collapse-<?php echo $accordionCounter; ?>"
               role="tabpanel"
               aria-labeledby="heading-<?php echo $accordionCounter; ?>"
               data-parent="#accordion" >
            <p class="lead">
              <?php the_sub_field( 'description' ); ?>
            </p>
          </div>

        </div>

      <?php $accordionCounter++; ?>
    <?php endwhile; ?>
  </div>
<?php endif; ?>

