<?php if ( have_rows( 'hero_section' ) ) : ?>
  <?php while ( have_rows( 'hero_section' ) ) : the_row(); ?>
    <?php
    $hero_background = get_sub_field( 'hero_background' );
    $hero_dark_theme = get_sub_field( 'hero_dark_theme' );
    $hero_classes = array( 'hero' );

    if ( $hero_background ) {
      array_push( $hero_classes, 'has-background' );
    }

    if ( $hero_dark_theme ) {
      array_push( $hero_classes, 'dark-theme' );
    }
    ?>
    <section class="<?php echo implode( $hero_classes, ' '); ?>"
        <?php if ( $hero_background ) : ?>
          style="background-image: url(<?php echo $hero_background['url']; ?>)"
        <?php endif; ?>
    >

      <div class="container hero-content">
        <?php
        if ( get_sub_field('hero_content') ) :
          the_sub_field( 'hero_content', false, false );
        endif;
        ?>
      </div>

    </section>
  <?php endwhile; ?>
<?php endif; ?>

