<?php if ( have_rows('cards') ) : ?>
  <div class="row">
    <?php while ( have_rows( 'cards' ) ) : the_row(); ?>
      <?php
      $card_image = get_sub_field( 'card_image' );
      $card_title = get_sub_field( 'card_title' );
      $card_subtitle = get_sub_field( 'card_subtitle' );
      $card_description = get_sub_field( 'card_description' );
      ?>

        <div class="col-md-6 mt-3">

          <div class="card">
            <img class="card-img-top" src="<?php echo $card_image['url']; ?>" alt="<?php echo $card_image['alt']; ?>" />

            <div class="card-body">
              <h4 class="card-title"><?php echo $card_title; ?></h4>
              <h6 class="card-subtitle"><?php echo $card_subtitle; ?></h6>
              <p class="card-text"><?php echo $card_description; ?></p>

              <?php if ( have_rows( 'card_social_links' ) ) : ?>
                <div class="float-right">
                  <ul class="social-links">
                  <?php while ( have_rows ( 'card_social_links') ) : the_row(); ?>
                    <li><a href="<?php the_sub_field( 'link' ); ?>" target="_blank"><?php the_sub_field( 'icon' ); ?></a></li>
                  <?php endwhile; ?>
                  </ul>
                </div>
              <?php endif; ?>
            </div>
          </div>

        </div>

    <?php endwhile; ?>
  </div>
<?php endif; ?>
