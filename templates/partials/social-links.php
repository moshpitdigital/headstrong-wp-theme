<ul class="social-links">
  <li><a href="https://www.youtube.com/channel/UCCDqOeIQj0uHtc2zzqao61w" target="_blank"><i class="fa social-icon fa-youtube" target="_blank"></i></a></li>
  <li><a href="https://www.facebook.com/headstrongfit" target="_blank"><i class="fa social-icon fa-facebook"></i></a></li>
  <li><a href="https://www.instagram.com/headstrongfit/" target="_blank"><i class="fa social-icon fa-instagram"></i></a></li>
</ul>
