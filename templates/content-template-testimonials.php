<section class="section">
  <h2><?php the_field('pre-testimonials_title'); ?></h2>
</section>

<div class="container">
  <?php if ( have_rows( 'testimonials' ) ) : ?>
      <div class="row">
        <?php while ( have_rows( 'testimonials' ) ) : the_row(); ?>
            <div class="col-md-6">
              <?php
              $testimonial_image = get_sub_field( 'testimonial_image' );
              $size = 'large';
              ?>

              <div class="d-flex h-100 align-items-center justify-content-center">
                <img src="<?php echo $testimonial_image['sizes'][$size]; ?>" alt="<?php echo $testimonial_image['alt']; ?>" class="img-fluid mt-4" />
              </div>
            </div>
        <?php endwhile; ?>
      </div>
  <?php endif; ?>
</div>

<section class="section">
  <h2><?php the_field('post-testimonials_title'); ?></h2>
</section>
