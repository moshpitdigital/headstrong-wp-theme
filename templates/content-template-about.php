<section class="section">
  <?php get_template_part('templates/partials/accordion'); ?>
</section>

<hr>

<section class="section">
  <h2>Meet the Team</h2>

  <?php get_template_part( 'templates/partials/cards' ); ?>
</section>
