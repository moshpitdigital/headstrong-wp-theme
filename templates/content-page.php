<?php get_template_part('templates/partials/hero'); ?>
<?php get_template_part( 'templates/content-template', get_post_field( 'post_name' )); ?>
<?php the_content(); ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
