<?php get_template_part('templates/page', 'header'); ?>

<div class="container mb-5">
  <?php get_template_part('templates/content-single', get_post_type()); ?>
</div>
