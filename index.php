<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="container mb-5">
  <?php while (have_posts()) : the_post(); ?>

    <div class="row mb-4">
      <?php if ( has_post_thumbnail() ) : ?>
        <div class="col-md-3">
          <a href="<?php the_permalink(); ?>" class="post-image-link opacity-hover">
            <?php the_post_thumbnail( 'medium' ); ?>
          </a>
        </div>

        <div class="col-md-9">
          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        </div>
      <?php else : ?>
        <div class="col-12">
          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        </div>
      <?php endif; ?>
    </div>

  <?php endwhile; ?>

  <?php echo get_the_posts_pagination( array( 'prev_text' => __( '&laquo;', 'textdomain' ), 'next_text' => __( '&raquo;', 'textdomain' )) ); ?>
</div>
